'use strict';

//data used for the select list 
const honorificData = [
    {
    id: 0,
    text: '<div style="font-size: 16px">  רופא.ה</div>',
    html: '<div style="font-size: 16px">  רופא.ה<img src="../assets/img/icons/icon-general-doctor.svg" style="padding:5px 10px 5px 10px; vertical-align: middle"></div>',
    title: '  רופא.ה'
  },
   {
    id: 1,
    text: '<div style="font-size: 16px"> אח.ות</div>',
    html: '<div style="font-size: 16px"> אח.ות<img src="../assets/img/icons/icon-general-nurse.svg" style="padding:5px 10px 5px 10px; vertical-align: middle"></div>',
    title: 'האח.ות'
  }, 
    {
    id: 2,
    text: '<div style="font-size: 16px"> רוקח.ת</div>',
    html: '<div style="font-size: 16px"> רוקח.ת<img src="../assets/img/icons/icon-general-pharmacist.svg" style="padding:5px 10px 5px 10px; vertical-align: middle"></div>',
    title: ' רוקח.ת'
  }
];


$(document).ready(function() { 
    //select2 library for custom dropdown menu
    $("#honorific").select2({
        data: honorificData,
        minimumResultsForSearch: Infinity,
        placeholder: 'התמחות',

        escapeMarkup: function(markup){
            return markup;
        },
        templateResult: function(data){
            return data.html;
        },
        templateSelection: function(data){
            return data.text;
        }
    });

    //flatpickr library for custom calendar in Hebrew, starting from Monday
    $("#dob").flatpickr({
        locale: "he",
        altInput: true,
        altFormat: 'Y, F j',
        dateFormat: "D"

    });
 });


//global variables for the HTML elements 

const pwd = document.getElementById('pwd');
const confirmPwd = document.getElementById('pwd__confirm');
const hidePwd = document.querySelector('.hide');
const confirmHidePwd = document.querySelector('.confirm__hide');
const form = document.getElementById('signupForm');
const submitBtn = document.getElementById('submit__btn');
const license = document.querySelector('.form__input--license');
const hint = document.querySelector('.form__input--hint');
const inputFields = document.querySelectorAll('input[required]');
const checkLabel = document.querySelector('.check__input');
const professionLabel = document.querySelector('.profession__label');
const gdprLabel = document.querySelector('.gdpr__label');

const firstName = document.getElementById('fname');
const lastName = document.getElementById('lname');
const email = document.getElementById('email');
const phone = document.getElementById('phone');
const profession = document.getElementById('profession');
const gdpr = document.getElementById('gdpr');


//check if the password and the confirmed password match 
const checkPwd = function () {
    if (pwd.value !== confirmPwd.value) {
        confirmPwd.setCustomValidity('Password does not match!');
        confirmPwd.classList.add('invalid');
    } else {
        confirmPwd.setCustomValidity(''); //indicates that there is no error 
        confirmPwd.classList.remove('invalid');

    }
}

pwd.addEventListener('focusout', checkPwd);
confirmPwd.addEventListener('focusout', checkPwd);

// toggle password visibility 
hidePwd.addEventListener('click', function (e) {
    const type = pwd.getAttribute('type') === 'password' ? 'text' : 'password';
    pwd.setAttribute('type', type);
    hidePwd.classList.toggle('show');

});

// confirm toggle password visibility 
confirmHidePwd.addEventListener('click', function (e) {
    const type = confirmPwd.getAttribute('type') === 'password' ? 'text' : 'password';
    confirmPwd.setAttribute('type', type);
    confirmHidePwd.classList.toggle('confirm__show');

});


//check if the required fields are filled with valid input data
const enableButton = function() {
    let inputLength = inputFields.length;
    for (let i = 0; i < inputLength; i++) {
        let current = inputFields[i];
        if (current.value === '' || current.classList.contains("invalid")){
            submitBtn.classList.remove("active");
            return;
        }
        else {
            submitBtn.classList.add("active");
            
        }
    }
}



//FIRST NAME VALIDATION 

firstName.addEventListener('focusout', function (e) {
    e.preventDefault();
    let string = firstName.value;
    console.log(string);
    let regex = /[A-Za-z]/;
    if ((regex.test(string))) {
        firstName.classList.remove('invalid');
    } else {
        firstName.classList.add('invalid');
    }
});


//LAST NAME VALIDATION 

lastName.addEventListener('focusout', function (e) {
    e.preventDefault();
    let string = lastName.value;
    console.log(string);
    let regex = /[A-Za-z]/;
    if ((regex.test(string))) {
        lastName.classList.remove('invalid');
        enableButton();
    } else {
        lastName.classList.add('invalid');
        enableButton();
    }
});

//EMAIL VALIDATION 
email.addEventListener('focusout', function (e) {
    e.preventDefault();
    let string = email.value;
    console.log(string);
    let regex = /[-a-zA-Z0-9~!$%^&amp;*_=+}{'?]+(\.[-a-zA-Z0-9~!$%^&amp;*_=+}{'?]+)*@([a-zA-Z0-9_][-a-zA-Z0-9_]*(\.[-a-zA-Z0-9_]+)*\.([cC][oO][mM]))(:[0-9]{1,5})?/;
    if ((regex.test(string))) {
        email.classList.remove('invalid');
        enableButton();
    } else {
        email.classList.add('invalid');
        enableButton();
    }
});

//LICENSE VALIDATION
license.addEventListener('focusout', function (e) {
    e.preventDefault();
    let string = license.value;
    console.log(string);
    let regex = /^\d{10}$/;
    if ((regex.test(string)) && string.length <= 10) {
        hint.innerHTML = 'אנא הכניסו את מספר הרישיון ללא המספר שמופיע לפני המקף (1-) המופיע במספר הרישיון';
        license.classList.remove('invalid');
        hint.classList.remove('wrong');
        enableButton();

    } else {
        hint.innerHTML = 'חסרים מספרים במספר הרשיון';
        hint.classList.add('wrong');
        license.classList.add('invalid');
        enableButton();
    }
});


//PHONE VALIDATION 
phone.addEventListener('focusout', function (e) {
    e.preventDefault();
    let string = phone.value;
    console.log(string);
    let regex = /^[0-9]*$/;
    if ((regex.test(string))) {
        phone.classList.remove('invalid');
        enableButton();
    } else {
        phone.classList.add('invalid');
        enableButton();
    }
});


//PASSWORD VALIDATION 
pwd.addEventListener('focusout', function (e) {
    e.preventDefault();
    let string = pwd.value;
    console.log(string);
    let regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/;
    if ((regex.test(string))) {
        pwd.classList.remove('invalid');
        enableButton();
    } else {
        pwd.classList.add('invalid');
        enableButton();
    }
});

//CONFIRM PASSWORD VALIDATION 
confirmPwd.addEventListener('focusout', function (e) {
    e.preventDefault();
    let string = confirmPwd.value;
    console.log(string);
    let regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/;
    if ((regex.test(string))) {
        console.log("correct confirm pwd");
        confirmPwd.classList.remove('invalid');
        enableButton();
    } else {
        console.log("WRONG confirm pwd");
        confirmPwd.classList.add('invalid');
        enableButton();
    }
});

//CHECKBOX VALIDATION
const checkboxValidate = function () {
    if (gdpr.checked && profession.checked === true) {
        console.log('Checkboxes are checked');
        enableButton();
        return true;
    } else {
        gdprLabel.style.color = "red";
        professionLabel.style.color = "red";
        console.log('You must check the boxes');
        return false;
    }
}



//redirect to confirmation page 
submitBtn.addEventListener('click', function (e) {
    if (submitBtn.classList.contains('active')) {
        window.location.href = "../confirmation.html";
    } else {
        e.preventDefault();
        alert('All fields must be filled out');
    }
});
